import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatColors{
   static const Color backgroundColor = Color.fromRGBO(248, 248, 248, 1);
   static const Color secondaryColor = Color(0xff003249);
   static const Color primaryColor =Color(0xff00cbc2);
   static const Color accentColor =  Color(0xffe5fffe);
   static const Color facebook = Color(0xFF485A96);
   static const Color google = Color(0xFF4885ED);

  

   static Color hexToColor(String hexString, {String alphaChannel = 'FF'}) {

     if (hexString.length == 4) {
       var r = hexString.substring(1,2) + hexString.substring(1,2);
       var g = hexString.substring(2,3) + hexString.substring(2,3);
       var b = hexString.substring(3,4) + hexString.substring(3,4);

       return Color(int.parse('0x$alphaChannel$r$g$b'));
     } else if (hexString.length == 7) {
       return Color(int.parse(hexString.replaceFirst('#', '0x$alphaChannel')));
     } else if (hexString.length == 9) {
       return Color(int.parse(hexString.replaceFirst('#', '0x')));
     }

     return ChatColors.secondaryColor;
   }
}