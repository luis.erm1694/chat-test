import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/state_bloc.dart';
import 'package:flutter_app/resources/colors.dart';
import 'package:flutter_app/routes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:username_gen/username_gen.dart';

class SigninPage extends StatefulWidget {
  const SigninPage({Key? key}) : super(key: key);


  @override
  State<SigninPage> createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  String userName = "Nick";
  TextEditingController nickController = TextEditingController();
  
  @override
  void initState() { 
    super.initState();
    userName = UsernameGen().generate();
    nickController.text = userName;
  }

  @override
  Widget build(BuildContext context) {
   ScreenUtil.defaultSize;
   
    return ScreenUtilInit(
      designSize:const Size(360, 690),

      builder: () => Scaffold(
        appBar: AppBar(
          backgroundColor: ChatColors.primaryColor,
          title: const Text("Chat App"),
        ),
        body: Center(
         
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              inputNick(),
              generateNickButton(),
              signinButton()
            ],
          ),
        ),
      ),
    );
  }

  Widget inputNick(){
    return  Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ChatColors.primaryColor.withOpacity(0.1)
      ),
      margin: const EdgeInsets.only(top:30),
      width: MediaQuery.of(context).size.width*.85,
      child: StreamBuilder<Object>(
        stream: blocState.nickUserStream,
        builder: (context, snapshot) {
          return TextFormField(
            controller: nickController,
            keyboardType: TextInputType.emailAddress,
            style:  TextStyle(color: ChatColors.secondaryColor, fontSize: 16.sp),
            scrollPadding: EdgeInsets.zero,
            decoration: const InputDecoration(
              contentPadding: EdgeInsets.only(top:17),
              border: InputBorder.none,
              prefixIcon:  Icon(Icons.account_circle),
            ),
            cursorColor: ChatColors.secondaryColor
          );
        }
      ),
    );
  }

  Widget generateNickButton(){
    return ElevatedButton(
      style: ElevatedButton.styleFrom(elevation: 2,),
      onPressed: () { 
        userName = UsernameGen().generate();
        nickController.text = userName;
        blocState.nickUser.sink.add(userName);
      },
      child: const Text('Crear nick'),
    );
  }

  Widget signinButton(){
    return TextButton(
      style: ElevatedButton.styleFrom(elevation: 2),
      onPressed: () {
        blocState.nickUser.sink.add(nickController.text);
        Navigator.pushNamed(context, AppRoutes.home);


      },
      child: const Text('Ingresar'),
    );
  }
}
