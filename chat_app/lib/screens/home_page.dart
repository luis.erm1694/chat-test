// ignore_for_file: avoid_print

import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/state_bloc.dart';
import 'package:flutter_app/resources/colors.dart';
import 'package:flutter_app/widgets/item_message.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:username_gen/username_gen.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String nickName = "Nick";
  TextEditingController msgController = TextEditingController();
  ScrollController scrollController =  ScrollController();
   Query? query;
   Stream<QuerySnapshot>? streamChat;
  String chatChannel = "group-finish";

  @override
  void initState() { 
    WidgetsBinding.instance!
        .addPostFrameCallback((_){
          setState(() {
             Timer(
              const Duration(milliseconds: 400),
              () {
                if(scrollController.hasClients) scrollController.jumpTo(scrollController.position.maxScrollExtent);
              } 
             );
           });
        });
    nickName = UsernameGen().generate();
    blocState.nickUser.sink.add(nickName);
    streamChat = FirebaseFirestore.instance.collection(chatChannel).snapshots();
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize:const Size(360, 690),
      builder: () =>  Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(MediaQuery.of(context).size.height*.1),
          child: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            elevation: 0,
            automaticallyImplyLeading: false,
            title:Align(
              alignment: Alignment.topLeft, 
              child: Padding(
                padding: EdgeInsets.only(bottom:8.h),
                child:  GestureDetector(
                  child:  Icon(Icons.chevron_left, color: ChatColors.secondaryColor, size: 30.h,),
                  onTap: (){
                    Navigator.pop(context);
                  },
                ),
              )
            ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(10.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(alignment: Alignment.topLeft, child: Container(
                    width: MediaQuery.of(context).size.width*.8,
                    padding: EdgeInsets.only(bottom:15.h, left: 25.w,),
                    child: StreamBuilder(
                      stream: blocState.nickUserStream,
                      initialData: "Nick",
                      builder: (context, AsyncSnapshot snapshot) {
                        return Text(snapshot.data, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.sp, color: ChatColors.secondaryColor),);
                      }
                    ),
                  )),
                  IconButton(
                    padding: EdgeInsets.only(bottom:15.h),
                    tooltip: "Edit Nick",
                    onPressed:() => blocState.changeNick(context),
                    icon: const Icon(Icons.edit)
                  )
                ],
              ),
            )
          ),
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height*.9,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              StreamBuilder<QuerySnapshot>(
                stream: streamChat,
                builder: (context, snapshot) {
                  if (!snapshot.hasData){ return Center(
                    child: Container(
                      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*.33),
                      width: 40.h,
                      height: 40.h,
                      child: const CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color> (ChatColors.primaryColor),)
                    ),
                  );
                  
                 }
                  return _chat(context, snapshot.data!.docs);
                }
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal:25.w, vertical: 20.h),
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  color: Color(0xfff6f6f6)
                ),
                child:  Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width*.7,
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.white,),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.only(left:12.0, top:10.h, bottom: 10),
                            child: TextField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              decoration: const InputDecoration.collapsed(hintText: "Escribe un mensaje"),
                              controller: msgController,
                            ),
                          )
                        ),
                      ),
                      InkWell(
                        child: const Icon(Icons.send, color:ChatColors.primaryColor),
                       onTap: ()async {
                         if(msgController.text.isEmpty){}
                         else{
                         try {
                           await FirebaseFirestore.instance.collection(chatChannel).doc(DateTime.now().toString()).set({
                           "msg": msgController.text,
                           "type": "msg",
                           "time": "${DateTime.now()}",
                           "nick": blocState.nickUser.value,
                           "id": BlocState.tokenFb,
             
                         });
                         msgController.clear();
                         //print(sendMsg.toString());
                         setState(() {
                             Timer(
                               const Duration(milliseconds: 300),
                               () => scrollController.jumpTo(scrollController.position.maxScrollExtent),
                          );
                         });
                          await blocState.sendPushMessage();
                           
                         } catch (e) {
                           print("ERROR AL ENVIAR MENSAJE");
                         }
                         }
                       })
                    ],
                ),
              )
            ],
          )
        ),
      ),
    );
  }

  Widget _chat(BuildContext context, List<QueryDocumentSnapshot> docs) {
    return Expanded(
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: docs.isEmpty?
        Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*.14),
            child: Text("Aún esta vacia esta conversación.",
              style: TextStyle(color: ChatColors.primaryColor, fontSize: 20.sp),
              textAlign: TextAlign.center,
            ),
          ),
        )
        
        : ListView.builder(
          //reverse: true,
          controller: scrollController,
          padding: EdgeInsets.symmetric(vertical:20.h),
          shrinkWrap: true,
          itemCount: docs.length,
          itemBuilder: (BuildContext context, int index) {
            //bool showDate = false;
          return ItemMessage( message : docs[index]);
         },
        ),
      ),
    );
  }

}
