// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/resources/colors.dart';
import 'package:flutter_app/widgets/custom_dialog.dart';
import 'package:flutter_app/widgets/rounded_button.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;


class BlocState{
  static String? tokenFb = "";
  TextEditingController nickController = TextEditingController();
  String valueNick = "";

  final nickUser = BehaviorSubject<String>();
  Stream<String> get nickUserStream => nickUser.stream;

  void changeNick(BuildContext context){
    Navigator.push(context, CustomDialog(
      
      child: Column(
       mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(height: 40),
        const Text("¿Deseas cambiar tu Nick?", style: TextStyle(fontFamily: "Avenir", fontSize: 17), textAlign: TextAlign.center,),
        const SizedBox(height: 30),
        inputNick(context),
        const SizedBox(height: 60),
        RoundedButton(color: ChatColors.secondaryColor, textColor: Colors.white, title: "Cancelar", isLoading: false,
          onPressed: (){Navigator.pop(context);},
        ),
        const SizedBox(height: 25),
        RoundedButton(color: ChatColors.primaryColor, textColor: Colors.white, title: "Aceptar", isLoading: false,
          onPressed:  (){
            if(valueNick != ""){
              nickUser.sink.add(valueNick);
              valueNick = "";
              Navigator.pop(context);
            }
              
          }
        ),
        const SizedBox(height: 40),
      ],
      ))
    );
  }

  Widget inputNick(BuildContext context){
    return  Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ChatColors.primaryColor.withOpacity(0.1)
      ),
      margin: const EdgeInsets.only(top:30, left: 30, right: 30),
      //padding: const EdgeInsets.symmetric(horizontal:30),
      width: MediaQuery.of(context).size.width*.85,
      child: TextFormField(
        //controller: nickController,
        onChanged: (value){
          valueNick = value;

        },
        keyboardType: TextInputType.name,
        style:  const TextStyle(color: ChatColors.secondaryColor, fontSize: 16),
        scrollPadding: EdgeInsets.zero,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(top:17),
          border: InputBorder.none,
          prefixIcon:  Icon(Icons.account_circle),
        ),
        cursorColor: ChatColors.secondaryColor
      )
       
    );
  }

  Future<void> sendPushMessage() async {
    if (BlocState.tokenFb! == "") {
      print('Unable to send FCM message, no token exists.');
      return;
    }
    try {
      await http.post(
        Uri.parse('https://api.rnfirebase.io/messaging/send'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          "Authorization" : "AAAAEJ0yXtM:APA91bHbsDgkddE0vlhnRJeY8sDFdXZID5C8SAKbj06ipB7Ax1O6wkRWUIqd8ni9WbUt6Nde9YmzzXstLu23u-xikn9CgMkquy16MyLa0ZjAkXSOIapRm_lq-t9jVJ7BDJlc0eUUBRJX"
        },
        body: constructFCMPayload(BlocState.tokenFb!),
      );
      print('FCM request for device sent!');
    } catch (e) {
      print(e);
    } 
  }

  String constructFCMPayload(String token) {
    int _messageCount = 0;
  _messageCount++;
  return jsonEncode({
    'token': token,
    'data': {
      'via': 'FlutterFire Cloud Messaging!!!',
      'count': _messageCount.toString(),
    },
    'notification': {
      'title': 'Hello FlutterFire!',
      'body': 'This notification (#) was created via FCM!',
      "priority" : "high"
    },
  });
}



  static final BlocState _bloc = BlocState._internal();

  factory BlocState() {
    return _bloc;
  }

  BlocState._internal();

  void dispose(){
    nickUser.close();
  }
}
final blocState = BlocState();