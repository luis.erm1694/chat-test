import 'package:flutter/material.dart';
import 'package:flutter_app/resources/colors.dart';

class RoundedButton extends StatelessWidget {
  final Color? color;
  final VoidCallback? onPressed;
  final bool isLoading;
  final String? title;
  final Color? textColor;
  final Color? loadingColor;
  final double? marginTop;
  final double? width;

  const RoundedButton({Key? key, this.color, this.onPressed, required this.isLoading, this.loadingColor, this.title, this.textColor, this.marginTop, this.width}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    return  Center(
      child: Container(
        height: 50,
        margin: EdgeInsets.only(top:marginTop??0),
        child: MaterialButton(
          height: 50,
          minWidth: width?? MediaQuery.of(context).size.width*.33,
          padding: const EdgeInsets.symmetric(vertical:12,),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          color:  color??ChatColors.secondaryColor,
          child:isLoading?
          SizedBox(width: 50, height: 50, 
            child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(loadingColor?? ChatColors.backgroundColor), strokeWidth: 3.2,),
          )
          :Text(title??"Continuar", style: TextStyle(fontSize: 15, color: textColor?? ChatColors.backgroundColor, fontWeight: FontWeight.w700, fontFamily: "gotham_bold", letterSpacing: 0.6)),
          onPressed: onPressed??(){}
        ),
      ),
      
    );
  }
}