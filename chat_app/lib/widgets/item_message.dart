import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/state_bloc.dart';
import 'package:flutter_app/resources/colors.dart';
import 'package:flutter_app/widgets/custom_card.dart';

class ItemMessage extends StatelessWidget {
 final QueryDocumentSnapshot message;

  const ItemMessage({Key? key, required this.message}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    bool isUser =  message["id"].toString().substring(0, 15) == BlocState.tokenFb!.substring(0, 15);

    return Align(
      alignment:isUser? Alignment.centerRight : Alignment.centerLeft,
      child: Column(
        children: [
          CustomCard(
            color: !isUser? ChatColors.accentColor : ChatColors.primaryColor,
            borderColor: !isUser? ChatColors.accentColor : ChatColors.primaryColor,
            shadowColor: !isUser? ChatColors.accentColor : Colors.grey[100],
            margin:isUser? const EdgeInsets.only(top:20, right: 20) : const EdgeInsets.only(top:20, left: 20),
            width: MediaQuery.of(context).size.width*.7,
            contentCard: Padding(
              padding: const EdgeInsets.only(left:18, right: 18, bottom: 7, top: 12),
              child: Column(
                crossAxisAlignment: !isUser? CrossAxisAlignment.start : CrossAxisAlignment.end,
                children: [
                  isUser? Container() 
                    : Padding(
                      padding: const EdgeInsets.only(bottom:8),
                      child: Text(message["nick"]??"Nick".toString(), 
                        style: const TextStyle(color: ChatColors.primaryColor, fontWeight: FontWeight.bold, fontFamily: "iskry"),
                        textAlign: !isUser? TextAlign.start : TextAlign.end,
                      ),
                    ),
                  Text(message["msg"].toString(), textAlign: !isUser? TextAlign.start : TextAlign.end,),
                  const SizedBox(height:8)
                ],
              ),

            ),
          ),
        ],
      ),
    );
  }
}