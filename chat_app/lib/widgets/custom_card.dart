import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomCard extends StatelessWidget {
  final Widget contentCard;
  final double? width;
  final double? radiusCard;
  final GestureTapCallback? onPress;
  final Color? borderColor;
  final EdgeInsetsGeometry? margin;
  final Color? color;
  final Color? shadowColor;
  

  const CustomCard({Key? key, required this.contentCard, this.width, this.radiusCard, this.onPress, this.margin, this.borderColor, this.color, this.shadowColor}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      
      onTap: onPress?? (){},
      child: Container(
        margin: margin?? EdgeInsets.symmetric( horizontal: 28.w),
        width: width?? MediaQuery.of(context).size.width*.9,
        padding: EdgeInsets.zero,
        //height: MediaQuery.of(context).size.height*.25,
        decoration: BoxDecoration(
          color: color??Colors.white,
          border: Border.all(
            color: borderColor?? Theme.of(context).primaryColor,
            width: 1.5,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(radiusCard??20),
          ),
          boxShadow: [
            BoxShadow(
              color: shadowColor?? Theme.of(context).primaryColor.withOpacity(0.4),
              spreadRadius: 0,
              blurRadius: ScreenUtil().setHeight(3),
              offset: Offset(0, ScreenUtil().setHeight(1.4)), // changes position of shadow
            ),
          ],
        ),
        child: contentCard
               
      ),
    );
  }
}