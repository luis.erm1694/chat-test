import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/state_bloc.dart';
import 'package:flutter_app/resources/colors.dart';
import 'package:flutter_app/routes.dart';
import 'package:flutter_app/screens/home_page.dart';

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
 
  await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
   
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  

   FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
     FirebaseMessaging.instance.getToken().then((value) {
    BlocState.tokenFb = value;
  });
  FirebaseMessaging messaging = FirebaseMessaging.instance;

   await messaging.requestPermission(
    alert: true,
    announcement: true,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat Lerm',
      theme: ThemeData(
       primaryColor: ChatColors.primaryColor
        //primarySwatch: ChatColors.green,
      ),
      initialRoute: AppRoutes.home,
      routes: {
        //AppRoutes.signin : (BuildContext context) => const SigninPage(),
        AppRoutes.home : (BuildContext context) => const HomePage()


      },
    );
  }
}

